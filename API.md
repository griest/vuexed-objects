## Functions

<dl>
<dt><a href="#commit">commit(payload)</a></dt>
<dd><p>Calls the store&#39;s appropriate commit method.
Defaults to <code>(path, mutation, value) =&gt; store.commit(`${path}/${mutation}`, payload)</code>.</p>
</dd>
<dt><a href="#mutationBody">mutationBody(state, payload)</a></dt>
<dd><p>The mutation function.
Allows you to perform custom behavior when the mutation is called.</p>
</dd>
<dt><a href="#addObjectToState">addObjectToState(state, object)</a></dt>
<dd><p>Adds an object to the state.
Defaults to pushing objects to an <code>objects</code> property in the state.</p>
</dd>
<dt><a href="#hotUpdateModule">hotUpdateModule(module)</a></dt>
<dd><p>Updates the store&#39;s module to include the passed module in place of the original module.
Take care that the module is accessible by <a href="#commit">commit</a>.
If you wish to move the module, use a custom commit function when first creating the module.</p>
</dd>
<dt><a href="#buildModule">buildModule([options], [mutationBody], [hotUpdateModule], [addObjectToState])</a> ⇒ <code>object</code></dt>
<dd><p>Builds a module whose mutations are mapped to by the objects&#39; setters.
The main entrypoint.</p>
</dd>
<dt><a href="#addReactivity">addReactivity([options])</a> ⇒ <code>Array.&lt;string&gt;</code></dt>
<dd><p>Maps an object&#39;s properties to mutations.
Based on Vue&#39;s own <code>observe</code> implementation.</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#Payload">Payload</a> : <code>object</code></dt>
<dd><p>The payload passed into <a href="#commit">commit</a> and <a href="#mutationBody">mutationBody</a>.</p>
</dd>
</dl>

<a name="commit"></a>

## commit(payload)
Calls the store's appropriate commit method.
Defaults to ```(path, mutation, value) => store.commit(`${path}/${mutation}`, payload)```.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| payload | [<code>Payload</code>](#Payload) | The payload of the mutation. Useful for debugging purposes. |

<a name="mutationBody"></a>

## mutationBody(state, payload)
The mutation function.
Allows you to perform custom behavior when the mutation is called.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | The module state. |
| payload | [<code>Payload</code>](#Payload) | The payload of the mutation. Useful for debugging purposes. |

<a name="addObjectToState"></a>

## addObjectToState(state, object)
Adds an object to the state.
Defaults to pushing objects to an `objects` property in the state.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | The module state. |
| object | <code>object</code> | The object to add. |

<a name="hotUpdateModule"></a>

## hotUpdateModule(module)
Updates the store's module to include the passed module in place of the original module.
Take care that the module is accessible by [commit](#commit).
If you wish to move the module, use a custom commit function when first creating the module.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| module | <code>object</code> | The module to be hot updated into the store. |

<a name="buildModule"></a>

## buildModule([options], [mutationBody], [hotUpdateModule], [addObjectToState]) ⇒ <code>object</code>
Builds a module whose mutations are mapped to by the objects' setters.
The main entrypoint.

**Kind**: global function  
**Returns**: <code>object</code> - The built module.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [options] | <code>object</code> | <code>{}</code> |  |
| options.objects | <code>Array.&lt;object&gt;</code> |  | The objects whose setters should be mapped to the registered module's mutations. |
| [options.path] | <code>string</code> |  | The module path. Example: `'my/nested/module'`. |
| [options.store] | <code>function</code> |  | The vuex store. Required unless `options.commit` is passed. |
| [options.commit] | [<code>commit</code>](#commit) |  | A custom commit function. |
| [options.only] | <code>Array.&lt;string&gt;</code> |  | If this is passed, only the property keys in this list will be mapped to mutations. |
| [options.exclude] | <code>Array.&lt;string&gt;</code> |  | If this is passed, the property keys in this list will not be mapped to mutations. This will override any keys in `options.only`. |
| [mutationBody] | [<code>mutationBody</code>](#mutationBody) |  | An optional function to be called when the mutation is called. |
| [options.addMutationName] | <code>string</code> | <code>&quot;&#x27;$add&#x27;&quot;</code> | The name of the mutation used to add reactive objects to the module. Set this if the default will conflict with any enumerable properties of the objects. |
| [hotUpdateModule] | [<code>hotUpdateModule</code>](#hotUpdateModule) |  | A function to call when the mutations are updated. Not required for HMR. |
| [addObjectToState] | [<code>addObjectToState</code>](#addObjectToState) |  | A function that will be called to add the object to the state. |

<a name="addReactivity"></a>

## addReactivity([options]) ⇒ <code>Array.&lt;string&gt;</code>
Maps an object's properties to mutations.
Based on Vue's own `observe` implementation.

**Kind**: global function  
**Returns**: <code>Array.&lt;string&gt;</code> - A list of property keys that were mapped to mutations.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [options] | <code>object</code> | <code>{}</code> |  |
| options.object | <code>Array.&lt;object&gt;</code> |  | The object whose setters should be mapped to mutations. |
| [options.path] | <code>string</code> |  | The module path. Example: `'my/nested/module'`. |
| [options.commit] | [<code>commit</code>](#commit) |  | A custom commit function. |
| [options.only] | <code>Array.&lt;string&gt;</code> |  | If this is passed, only the property keys in this list will be mapped to mutations. |
| [options.exclude] | <code>Array.&lt;string&gt;</code> |  | If this is passed, the property keys in this list will not be mapped to mutations. This will override any keys in `options.only`. |

<a name="Payload"></a>

## Payload : <code>object</code>
The payload passed into [commit](#commit) and [mutationBody](#mutationBody).

**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| value | <code>any</code> | The value being set. |
| mutation | <code>string</code> | The name of the mutation. Corresponds to the property name of the original object(s). |
| path | <code>string</code> | The path to the module. |
| object | <code>object</code> | The object being mutated. |

