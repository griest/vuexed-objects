import uniq from 'lodash.uniq'
import difference from 'lodash.difference'

/**
 * The payload passed into {@link commit} and {@link mutationBody}.
 *
 * @typedef Payload
 * @global
 * @type {object}
 * @property {any} value The value being set.
 * @property {string} mutation The name of the mutation. Corresponds to the property name of the original object(s).
 * @property {string} path The path to the module.
 * @property {object} object The object being mutated.
 */

/**
 * Calls the store's appropriate commit method.
 * Defaults to ```(path, mutation, value) => store.commit(`${path}/${mutation}`, payload)```.
 *
 * @global
 * @function commit
 * @param {Payload} payload The payload of the mutation. Useful for debugging purposes.
 */

/**
 * The mutation function.
 * Allows you to perform custom behavior when the mutation is called.
 *
 * @global
 * @function mutationBody
 * @param {object} state The module state.
 * @param {Payload} payload The payload of the mutation. Useful for debugging purposes.
 */

/**
 * Adds an object to the state.
 * Defaults to pushing objects to an `objects` property in the state.
 *
 * @global
 * @function addObjectToState
 * @param {object} state The module state.
 * @param {object} object The object to add.
*/

/**
 * Updates the store's module to include the passed module in place of the original module.
 * Take care that the module is accessible by {@link commit}.
 * If you wish to move the module, use a custom commit function when first creating the module.
 *
 * @global
 * @function hotUpdateModule
 * @param {object} module The module to be hot updated into the store.
 */

/**
 * Builds a module whose mutations are mapped to by the objects' setters.
 * The main entrypoint.
 *
 * @param {object} [options={}]
 * @param {object[]} options.objects The objects whose setters should be mapped to the registered module's mutations.
 * @param {string} [options.path] The module path. Example: `'my/nested/module'`.
 * @param {function} [options.store] The vuex store. Required unless `options.commit` is passed.
 * @param {commit} [options.commit] A custom commit function.
 * @param {string[]} [options.only] If this is passed, only the property keys in this list will be mapped to mutations.
 * @param {string[]} [options.exclude] If this is passed, the property keys in this list will not be mapped to mutations. This will override any keys in `options.only`.
 * @param {mutationBody} [mutationBody] An optional function to be called when the mutation is called.
 * @param {string} [options.addMutationName='$add'] The name of the mutation used to add reactive objects to the module. Set this if the default will conflict with any enumerable properties of the objects.
 * @param {hotUpdateModule} [hotUpdateModule] A function to call when the mutations are updated. Not required for HMR.
 * @param {addObjectToState} [addObjectToState] A function that will be called to add the object to the state.
 *
 * @return {object} The built module.
 */
export function buildModule ({
  objects,
  path,
  commit,
  store,
  only,
  exclude,
  mutationBody,
  addMutationName,
  hotUpdateModule,
  addObjectToState,
} = {}) {
  const mutations = {}
  let fields = []
  const initialState = {}
  const commit_ = commit || (({path, mutation, value, object}) => store.commit(`${path}/${mutation}`, {path, mutation, value, object}))
  const addObjectToState_ = addObjectToState || ((state, obj) => {
    if (!state.objects) state.objects = []
    state.objects.push(obj)
  })
  const mutationBody_ = mutationBody || (() => {})

  function addMutation (name) {
    mutations[name] = mutationBody_
  }

  function addReactivityToObject (object) {
    return addReactivity({
      object,
      path,
      commit: commit_,
      only,
      exclude,
    })
  }

  function vuexObjects (state, objects) {
    // build a list of keys corresponding to the reactive fields just created on the objects
    const keys = uniq(objects.map(addReactivityToObject).flat())

    // create mutations for each key
    const diff = difference(keys, fields)
    if (diff.length > 0) {
      fields.concat(diff)
      diff.forEach(addMutation)
    }

    objects.forEach(obj => addObjectToState_(state, obj))

    return diff
  }

  function addObjects (state, objOrObjs) {
    const objs = Array.isArray(objOrObjs) ? objOrObjs : [objOrObjs]
    const diff = vuexObjects(state, objs)
    if (hotUpdateModule && diff.length > 0) hotUpdateModule(createModule(state))
    objs.forEach(o => mutationBody_(state, {object: o}))
  }

  function createModule (state = {}) {
    return {
      namespaced: true,
      mutations: {
        ...mutations,
        [addMutationName || '$add']: addObjects,
      },
      state: () => state,
    }
  }

  // init state with reactive objects
  if (objects) vuexObjects(initialState, objects)

  return createModule(initialState)
}

/**
 * Maps an object's properties to mutations.
 * Based on Vue's own `observe` implementation.
 *
 * @param {object} [options={}]
 * @param {object[]} options.object The object whose setters should be mapped to mutations.
 * @param {string} [options.path] The module path. Example: `'my/nested/module'`.
 * @param {commit} [options.commit] A custom commit function.
 * @param {string[]} [options.only] If this is passed, only the property keys in this list will be mapped to mutations.
 * @param {string[]} [options.exclude] If this is passed, the property keys in this list will not be mapped to mutations. This will override any keys in `options.only`.
 *
 * @returns {string[]} A list of property keys that were mapped to mutations.
 */
export function addReactivity ({
  object,
  path,
  commit,
  only,
  exclude,
} = {}) {
  let keys = Object.keys(object)

  if (only) {
    keys = keys.filter(k => only.includes(k))
  }

  if (exclude) {
    keys = keys.filter(k => !exclude.includes(k))
  }

  keys.forEach(key => {
    let val, set

    const property = Object.getOwnPropertyDescriptor(object, key)
    if (property && property.configurable === false) {
      return
    }

    // cater for pre-defined getter/setters
    const getter = property && property.get
    const setter = property && property.set
    if (!getter || setter) {
      val = object[key]
    }

    // do all if checks while defining the setter, instead of inside it, should help performance a little
    if (setter) {
      set = function reactiveSetter (v) {
        setter.call(object, v)
        commit({path, mutation: key, value: v, object})
      }
    } else {
      set = function reactiveSetter (v) {
        val = v
        commit({path, mutation: key, value: v, object})
      }
    }
    Object.defineProperty(object, key, {
      enumerable: true,
      configurable: true,
      get: () => getter ? getter.call(object) : val,
      set,
    })
  })

  return keys
}
